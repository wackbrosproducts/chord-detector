import { ChordDetectorPage } from './app.po';

describe('chord-detector App', function() {
  let page: ChordDetectorPage;

  beforeEach(() => {
    page = new ChordDetectorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
