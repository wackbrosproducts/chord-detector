# Chord Detector #

This is a WackoBro Chord Detector experimental Project. 

### What is this repository for? ###

* This repository currently consists of first few lines of code to get us started in the path
  of building a usable Chord Detecting Mobile Application. We will try and use the native 
  Capabilities of the browser to access the microphone and gain the sound emitted by the outside 
  world and figure out the closest chords
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download the repository in your local machine
* Install nodejs using the information given in `https://nodejs.org/en/download/package-manager`
* Install Angalar 2 by using the command `npm install -g @angular/cli@latest`
* Go into your repository using the terminal/cmd and run the command `npm install`
* After completing all the steps, go into the repository using the terminal/cmd and run `ng serve`, then open the browser window http://localhost:4200 to see the result.

### Contribution guidelines ###

* After downloading the project and setting up git. Create a branch with a name feature/*feature you want to create/add*
* After completing the feature and writing test cases for it push the feature to the repository
* Raise a `pull request` to the master

### Who do I talk to? ###

* Paul Thomas
* Ashish Thomas
